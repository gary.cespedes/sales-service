package com.dh.sales.service.model.domain;

import javax.persistence.*;

/**
 * @author Gary A. Cespedes
 **/
@Entity
@Table(name = "employee_table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "employeeid", referencedColumnName = "personid")
})
public class Employee extends Person{

    @Column(name = "position", nullable = false)
    private String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}