package com.dh.sales.service.model.repositories;

import com.group.service.sales.demo.model.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Gary A. Cespedes
 **/
public interface ClientRepository extends JpaRepository<Client, Long> {
}
