package com.dh.sales.service.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Gary A. Cespedes
 **/
@Configuration
@PropertySource("classpath:/configuration/api-version.properties")
public class EmployeeMyProperties {

    @Value("${users.state:DESACTIVE}")
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
