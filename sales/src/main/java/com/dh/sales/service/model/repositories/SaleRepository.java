package com.dh.sales.service.model.repositories;

import com.group.service.sales.demo.model.domain.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Gary A. Cespedes
 **/
public interface SaleRepository extends JpaRepository<Sale, Long> {
}
