package com.dh.sales.service.service;

import com.group.service.sales.demo.config.EmployeeMyProperties;
import com.group.service.sales.demo.input.EmployeeCreateInput;
import com.group.service.sales.demo.model.domain.Employee;
import com.group.service.sales.demo.model.domain.Male;
import com.group.service.sales.demo.model.repositories.EmployeeRepository;
import com.group.service.sales.demo.model.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Gary A. Cespedes
 **/

@Scope("prototype")
@Service
public class EmployeeCreateService {

    private EmployeeCreateInput input;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private SaleRepository saleRepository;

    @Autowired
    private EmployeeMyProperties employeeMyProperties;

    private Employee employee;

    public void execute(){
      //  Sale sale = composeSaleInstance();
      //  sale = saleRepository.save(sale);

        Employee employeeInstance = composeEmployeeInstance();
        employee = employeeRepository.save(employeeInstance);
    }

    /*private Sale composeSaleInstance(){
        Sale instance = new Sale();
        instance.set
    }*/
    private Employee composeEmployeeInstance(){
        Employee instance = new Employee();
        instance.setFiristName(input.getFiristName());
        instance.setLastName(input.getLastName());
        instance.setEmail(input.getEmail());
        instance.setPosition(input.getPosition());
        //instance.setMale(Male.MAN);
        Male male = Male.MAN;
        Boolean isState = state();
        if (isState){
            male = Male.WOMAN;
        }
        instance.setMale(male);
        instance.setDelete(Boolean.FALSE);
        instance.setCreatedDate(new Date());

        return instance;
    }

    private Boolean state(){
        return input.getState()== employeeMyProperties.getState();
    }

    public void setInput(EmployeeCreateInput input){
        this.input = input;
    }

    public Employee getEmployee(){
        return employee;
    }

}
