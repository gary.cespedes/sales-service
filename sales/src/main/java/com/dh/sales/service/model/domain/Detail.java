package com.dh.sales.service.model.domain;

import javax.persistence.*;

/**
 * @author Gary A. Cespedes
 **/
@Entity
@Table(name = "detail_table")
public class Detail {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "totalProducts", nullable = false)
    private Integer totalProducts;

    @Column(name = "totalPrice", nullable = false)
    private Long totalPrice;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "detailsale", referencedColumnName = "saleid", nullable = false)
    private Sale sale;

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(Integer totalProducts) {
        this.totalProducts = totalProducts;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }
}
