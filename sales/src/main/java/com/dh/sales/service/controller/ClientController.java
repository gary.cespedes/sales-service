package com.dh.sales.service.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Gary A. Cespedes
 **/
@Api(
        tags = "Client rest",
        description = "Operations over clients"
)
@RestController
@RequestMapping("/clients")
@RequestScope
public class ClientController {
}