package com.dh.sales.service.controller;

import com.group.service.sales.demo.input.EmployeeCreateInput;
import com.group.service.sales.demo.model.domain.Employee;
import com.group.service.sales.demo.service.EmployeeCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Gary A. Cespedes
 **/
@Api(value="Employee Management System",
        description="Operations pertaining to employee in Employee Management System")
@RestController
@RequestMapping("/employee")
@RequestScope
public class EmployeeController {

    @Autowired
    private EmployeeCreateService employeeCreateService;

    @ApiOperation(
            value = "Create a employee"
    )
    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeCreateInput input) {
        employeeCreateService.setInput(input);
        employeeCreateService.execute();

        return employeeCreateService.getEmployee();

    }
}
