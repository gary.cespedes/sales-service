package com.dh.sales.service;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Gary A. Cespedes
 **/
@Configuration
@EnableFeignClients
@ComponentScan("com.dh.sales.service")
public class Config {
}
