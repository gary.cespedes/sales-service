package com.dh.sales.service.input;

/**
 * @author Gary A. Cespedes
 **/
public class EmployeeCreateInput {

    private String firistName;

    private String lastName;

    private String position;

    private String email;

    private String state;

    public String getState() {
        return state;
    }

    public String getFiristName() {
        return firistName;
    }

    public void setFiristName(String firistName) {
        this.firistName = firistName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
