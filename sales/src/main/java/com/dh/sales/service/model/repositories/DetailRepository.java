package com.dh.sales.service.model.repositories;

import com.group.service.sales.demo.model.domain.Detail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Gary A. Cespedes
 **/
public interface DetailRepository extends JpaRepository<Detail, Long> {
}
