package com.dh.sales.service.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author Gary A. Cespedes
 **/
@Api(
        tags = "Sale",
        description = "Operations over sales"
)
@RestController
@RequestScope
public class SaleController {
}
