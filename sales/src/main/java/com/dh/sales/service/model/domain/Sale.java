package com.dh.sales.service.model.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Gary A. Cespedes
 **/
@Entity
@Table(name = "sale_table")
@ApiModel(description = "All details about the account")
public class Sale {

    @Id
    @Column(name = "saleid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @ApiModelProperty(notes = "The database generated Sale ID")
    private Long id;

    @Column(name = "numberSale", length = 100, nullable = false)
    private Long numberSale;

    @Column(name = "createDate", nullable = false)
    private Date createDate;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "saleclientid", referencedColumnName = "clientid", nullable = false)
    private Client client;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "saleemployeeid", referencedColumnName = "employeeid", nullable = false)
    private Employee employee;

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumberSale() {
        return numberSale;
    }

    public void setNumberSale(Long numberSale) {
        this.numberSale = numberSale;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
