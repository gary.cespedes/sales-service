package com.dh.sales.service.model.domain;

/**
 * @author Gary A. Cespedes
 **/
public enum Male {
    WOMAN,
    MAN
}
